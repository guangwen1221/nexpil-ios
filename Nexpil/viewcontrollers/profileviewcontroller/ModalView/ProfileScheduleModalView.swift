//
//  ProfileScheduleModalView.swift
//  Nexpil
//
//  Created by Guang on 12/16/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Alamofire

class ProfileScheduleModalView: UIView,  TTRangeSliderDelegate{

    @IBOutlet weak var rangeSlider: TTRangeSlider!
    @IBOutlet weak var awakeTimeLabel: UILabel!
    @IBOutlet weak var awakeNoonLabel: UILabel!
    @IBOutlet weak var asleepTimeLabel: UILabel!
    @IBOutlet weak var asleepNoonLabel: UILabel!
    
    var delegate: ProfileScheduleDetailModalViewDelegate?
    var type = 0
    var data: NSDictionary? {
        didSet {
            let timeStart = Float(data!.value(forKey: "timeStart") as! String)!
            let timeEnd = Float(data!.value(forKey: "timeEnd") as! String)!
            rangeSlider.selectedMinimum = timeStart
            rangeSlider.selectedMaximum = timeEnd
            
            self.updateTimeLabel(timeStart, timeEnd)
        }
    }
    
    private var startTime = 0
    private var endTime = 0
    
    override func awakeFromNib() {
        rangeSlider.minHandleColor = UIColor(hex: "39D3E3")
        rangeSlider.maxHandleColor = UIColor(hex: "4939E3")
        rangeSlider.delegate = self
        
    }
    
    @IBAction func closeDialog(_ sender: Any) {
        self.delegate?.popScheduleDetailViewDismissal()
    }
    
    @IBAction func saveSchedule(_ sender: Any) {
        let params = [
            "userid": PreferenceHelper().getId(),
            "startTime": startTime,
            "endTime": endTime,
            "type": type,
            "choice": 0
        ]

        Alamofire.request(DataUtils.APIURL + DataUtils.PATIENT_URL, method: .post, parameters: params, encoding: URLEncoding()).responseString { response in
            self.delegate?.popScheduleDetailViewDismissal()
        }
    }
    
    func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
        self.updateTimeLabel(selectedMinimum, selectedMaximum)
    }
    
    private func updateTimeLabel(_ selectedMinimum: Float, _ selectedMaximum: Float) {
        let minValue = Int(selectedMinimum)
        let maxValue = Int(selectedMaximum)
        
        self.awakeTimeLabel.text = "\(minValue > 11 ? minValue - 12 : minValue)";
        self.awakeNoonLabel.text = minValue > 11 ? "PM" : "AM"
        if minValue == 12 || minValue == 0 {
            self.awakeTimeLabel.text = "12"
        }
        
        self.asleepTimeLabel.text = "\(maxValue > 11 ? maxValue - 12 : maxValue)";
        if maxValue == 12 {
            self.asleepTimeLabel.text = "12"
        }
        self.asleepNoonLabel.text = maxValue > 11 ? "PM" : "AM"
        if maxValue == 24 {
            self.asleepNoonLabel.text = "AM"
        }
        
        self.startTime = minValue
        self.endTime = maxValue
    }
}
