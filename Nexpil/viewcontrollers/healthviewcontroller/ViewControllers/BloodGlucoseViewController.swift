//
//  BloodGlucoseViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKitUI

class BloodGlucoseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let chartView = OCKCartesianChartView(type: .bar)
        chartView.headerView.titleLabel.text = "Trends"
        chartView.headerView.detailLabel.text = "Description"
        chartView.graphView.dataSeries = [
            OCKDataSeries(values: [0, 1, 1, 2, 3, 3, 2], title: "Doxylamine")
        ]
        
        self.view.addSubview(chartView)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
